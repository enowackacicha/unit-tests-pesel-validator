package agh.qa;

public class Pesel {
    private byte[] pesel;

    public Pesel(byte[] pesel){
        this.pesel = pesel;
    }

    public int getBirthYear(){
        int year = getYear();
        int month = getMonth();

        if (month > 80 && month < 93) {
            year += 1800;
        }
        else if (month > 0 && month < 12) {
            year += 1900;
        }
        else if (month > 20 && month < 32) {
            year += 2000;
        }
        else if (month > 40 && month < 52) {
            year += 2100;
        }
        else if (month > 60 && month < 72) {
            year += 2200;
        }
        return year;
    }

    public int getBirthMonth() {
        int month = getMonth();

        if (month > 80 && month < 93) {
            month -= 80;
        }
        else if (month > 20 && month < 33) {
            month -= 20;
        }
        else if (month > 40 && month < 53) {
            month -= 40;
        }
        else if (month > 60 && month < 73) {
            month -= 60;
        }
        return month;
    }

    public int getBirthDay(){
        return 10 * pesel[4] + pesel[5];
    }

    public int getCheckSum(){
        return pesel[10];
    }

    public byte getDigit(int index){
        if(index < 0 || index > pesel.length)
            return -1;

        return pesel[index];
    }

    private int getYear(){
        return 10 * pesel[0] + pesel[1];
    }

    private int getMonth(){
        return 10 * pesel[2] + pesel[3];
    }
}
